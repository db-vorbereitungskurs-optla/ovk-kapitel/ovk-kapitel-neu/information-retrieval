# HTML Template

Template Projekt, welches das Design des OVK Kurses Informatik nutzt

## Installation
```bash
npm install
```

## Entwicklung
Mit
```bash
source start.sh
```
kann die Entwicklungskonfiguration von webpack gestartet werden

Mit einem `?o=0` am Ende der URL kann die versteckte Navigation über das Hamburger-Menü aufgerufen werden:
http://localhost:9000/index.html?o=0

### Generierung der Navigation aus der nav.json

Über die nav.json kann die Ordnung, in der die Inhalte dargestellt werden sollen, angepasst werden. Die File-Elemente enthalten nur das Attribut **name**, mit dem die Bezeichnung des Buttons festgelegt wird, um den entsprechenden Inhalt aufzurufen. Daraus wird automatisch der Dateiname generiert. Bei der Generierung werden Leerzeichen und Umlaute entsprechend ersetzt, sodass aus der Bezeichnung "Schlüssel in Datenbanken" der zugehörige Dateiname "Schluessel_in_Datenbanken" erstellt wird.
Ein Directory-Element enhält die u.a. die Attribute **dir_name** und **btn_name**. Da die Bezeichnung des Buttons **btn_name** teilweise stark von der Bezeichnung des eigentlichen Ordners **dir_name** teils stark abweichen kann, müssen für das Directory-Element beide Bezeichnungen festgelegt werden.

Mit
```bash
python src/helpers/nav_builder.py
```
wird aus der nav.json die Navigation generiert und in nav.handlebars geschrieben. 
Aktuell wird die Generierung nicht automatisch beim Starten der Entwicklungskonfiguration ausgeführt, sondern muss vorher erfolgen.

## Produktiv-Test
Mit
```bash
npm run start
```
werden die Dateien analog zu einem Produktivsystem gebaut und lokal verfügbar gemacht.


