<!doctype html>
<html lang="de-DE">

{{> head }}

<body>
<div>
    {{> breadcrumbs }}
    <div id="ovk-content">
        <h1>Information Retrieval</h1>
        <p>
            Information Retrieval (IR) bezeichnet das <b>Auffinden relevanter Informationen in großen Sammlungen
            unstrukturierten Materials</b>, meist auf Computersystemen gespeicherten Textdokumenten. Das Ziel von IR ist es,
            relevante Informationen effizient und präzise zu extrahieren, um die Suchanforderungen von Nutzer:innen zu
            erfüllen. Dies umfasst die Entwicklung und Anwendung von Techniken zur <b>Indexierung, Suche und Bewertung</b> von
            Daten. IR ist in vielen Informationssystemen, wie Suchmaschinen, digitalen Bibliotheken und Datenbanken, von
            entscheidender Bedeutung und trägt wesentlich dazu bei, große Datenmengen zugänglich und nutzbar zu machen.
        </p>

        <details>
            <summary><span>Boolean Retrieval</span></summary>

            <center>
                <figure style="background-color: inherit; padding-right: 2em;">
                    <table style="width: auto">
                        <tr>
                            <th></th>
                            <th>$D_1$</th>
                            <th>$D_2$</th>
                            <th>$D_3$</th>
                            <th>$D_4$</th>
                        </tr>
                        <tr>
                            <td><em>romeo</em></td>
                            <td>1</td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>juliet</em></td>
                            <td>1</td>
                            <td>1</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>happy</em></td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>live</em></td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>die</em></td>
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>free</em></td>
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                            <td>0</td>
                        </tr>
                        <tr>
                            <td><em>new-hampshire</em></td>
                            <td>0</td>
                            <td>0</td>
                            <td>1</td>
                            <td>1</td>
                        </tr>
                    </table>
                    <br>
                    <caption>
                        <center>Term-Incidence Matrix</center>
                    </caption>
                </figure>

                <figure style="background-color: inherit; padding-left: 2em;">
                    <table>
                        <tr>
                            <th>Dictionary</th>
                            <th>Postings</th>
                        </tr>
                        <tr>
                            <td><em>romeo</em></td>
                            <td>$D_1$, $D_3$</td>
                        </tr>
                        <tr>
                            <td><em>juliet</em></td>
                            <td>$D_1$, $D_2$</td>
                        </tr>
                        <tr>
                            <td><em>happy</em></td>
                            <td>$D_2$</td>
                        </tr>
                        <tr>
                            <td><em>live</em></td>
                            <td>$D_2$</td>
                        </tr>
                        <tr>
                            <td><em>die</em></td>
                            <td>$D_3$</td>
                        </tr>
                        <tr>
                            <td><em>free</em></td>
                            <td>$D_3$</td>
                        </tr>
                        <tr>
                            <td><em>new-hampshire</em></td>
                            <td>$D_3$, $D_4$</td>
                        </tr>
                    </table>
                    <br>
                    <caption>
                        <center>Inverted Index</center>
                    </caption>
                </figure>
            </center>

            <p>
                Boolean Retrieval bezeichnet eine einfache Form des IR, bei der die <b>Stichwörter einer Suchanfrage
                (Tokens)</b> mittels boolescher Grundoperatoren (UND, ODER, NICHT) miteinander verknüpft werden.<br>
                In der grundlegendsten Form erfolgt der Abgleich mithilfe einer sogenannten <b>Term-Document Incidence
                Matrix</b>. Dabei werden die einzelnen Tokens der Datenbasis den jeweiligen Dokumenten
                gegenübergestellt. Zur Erstellung dieser Matrix werden die Tokens in den Dokumenten der Datenbasis
                extrahiert und ihr Vorkommen mit dem Wert 1 in der entsprechenden Zelle der Matrix markiert. Für jeden
                möglichen Suchbegriff existiert anschließend ein <b>Incident-Vektor</b>, der das Vorkommen des
                entsprechenden Tokens in den einzelnen Dokumenten der Datenbasis anzeigt. Indem die logisch verknüpften
                Begriffe der Suchanfrage durch ihre jeweiligen Incident-Vektoren ersetzt werden, kann eine Liste von
                Dokumenten zurückgegeben werden, die die gewünschte Verknüpfung der Suchbegriffe enthalten.
            </p>

            <div class="card ovk-hinweis">
                <div class="card-header"><b>Beispiel:</b> Suchanfrage $\text{romeo} \wedge \text{juliet} \wedge \neg
                    \text{happy}$
                </div>
                <div class="card-body">
                    <b>Incident-Vektoren:</b><br>
                    $\text{romeo} = (1,0,1,0)$ <br>
                    $\text{juliet} = (1,1,0,0)$ <br>
                    $\text{happy} = (0,1,0,0) \Rightarrow \neg \text{happy} = (1,0,1,1)$ <br>
                    <br>
                    <b>Logische Verknüpfung:</b><br>
                    $(1,0,1,0) \wedge (1,1,0,0) \wedge (1,0,1,1) = (1,0,0,0) \rightarrow D_1$
                </div>
            </div>

            <p>Eine zentrale Problemstellung der Verwendung von Term-Document-Incidence-Matrizen besteht in ihrem hohen
                Speicherbedarf bei gleichzeitig geringer -effizienz:</p>

            <div class="card ovk-hinweis">
                <div class="card-header"><b>Beispiel:</b> Effizienz von Term-Document-Incidence-Matrizen</div>
                <div class="card-body">
                    Eine Datenbasis besteht aus insgesamt $N=10^6$ Dokumente, welche durchschnittlich rund $n = 10^3$
                    Tokens enthalten. Jeder Token besitzt eine durchschnittliche Länge von $d = 6\;\text{Byte}$. Es
                    ergibt sich ein Speicherbedarf von $D = N \cdot n \cdot d = 6\;\text{GB}$ für die Datenbasis. Unter
                    der Annahme, dass unter der Gesamtmenge aller Tokens insgesamt $M = 500.000$ individuelle
                    Wörter/Terme existieren, beträgt die Größe der Term-Document-Incidence-Matrix selbst für diese
                    verhältnismäßig kleine Datenbasis $T = M \cdot N \cdot 1\;\text{Bit} = 62,5\;\text{GB}$. Rechnerisch
                    kann die maximale Besetzung der Matrix bei $\frac{n \cdot N}{T} = 0,2\%$ liegen – die Matrix ist
                    also nahezu vollständig unbesetzt, was auf eine äußerst ineffiziente Speichernutzung hinweist.
                </div>
            </div>

            <p>Moderne Boolean-Retrieval-Systeme arbeiten daher mit einer effizienteren Form der Indizierung – dem
                sogenannten <b>Inverted Index</b>. Im Gegensatz zur vollständigen Term-Document-Incidence-Matrix wird hierbei
                für jeden individuellen Token lediglich eine Liste aller Dokumente gespeichert, die diesen Token
                enthalten. Die Menge aller verfügbaren Tokens wird dabei als <b>Dictionary</b> und die Dokumentlisten der
                Tokens als <b>Postings</b> bezeichnet. Um eine hohe Verfügbarkeit und schnelle Durchsuchbarkeit zu
                gewährleisten, werden das Dictionary und die Verweise auf die jeweiligen Postings meist dauerhaft im
                Arbeitsspeicher des IR-Systems gehalten.</p>
        </details>

        <details>
            <summary><span>Ranking von Suchergebnissen</span></summary>

            <p>Eine zentrale Herausforderung bei der Nutzung von Boolean-Retrieval-Systemen besteht in der Formulierung
                präziser Suchanfragen. Um eine sinnvolle Anzahl an Suchergebnissen zu erhalten, müssen die einzelnen
                Tokens einer Suchanfrage geschickt durch <b>logische Operationen</b> miteinander verknüpft werden. Für die
                meisten alltäglichen Anwendungsfälle ist diese Form der Suche jedoch nicht praktikabel. Moderne
                IR-Systeme verwenden daher häufig sogenannte <b>Ranking-Algorithmen</b>. Diese Algorithmen bewerten die
                Übereinstimmung zwischen einer gestellten Suchanfrage und den einzelnen Dokumenten in der Datenbank
                anhand verschiedener Metriken und geben die Ergebnisse in absteigender Reihenfolge der Relevanz aus.</p>

            <details>
                <summary><span>Jaccard-Koeffizient</span></summary>

                <p>Eine einfache Möglichkeit zur Berechnung der Relevanz des Inhalts (individuelle Tokens) eines
                    Dokuments $d$ und einer Suchanfrage $q$ kann mittels des Jaccard-Koeffizienten berechnet werden:</p>
                <center>$$J_{q,d} = \frac{|d \cap q|}{|d \cup q|}$$</center>
                <p>Der Jaccard-Koeffizient misst den <b>Übereinstimmungsgrad zwischen den Elementen zweier Mengen</b>. Sind die
                    Suchanfrage $q$ und das betrachtete Dokument $d$ vollkommen disjunkt, beträgt $J_{q,d} = 0$. Stimmen
                    beide Mengen genau überein, beträgt $J_{q,d} = 1$. </p>
                <p>Zum Ranking der Suchergebnisse in einem IR-System können die Jaccard-Koeffizienten aller Dokumente
                    der Datenbasis in Bezug auf eine Suchanfrage berechnet und anschließend in absteigender Reihenfolge
                    sortiert werden.</p>
                <p>Ein wesentlicher Nachteil des Rankings von Dokumenten mittels des Jaccard-Koeffizienten besteht in
                    der <b>Nichtberücksichtigung der Auftretenshäufigkeit</b> der enthaltenen Tokens. Allgemein gilt jedoch: je
                    häufiger ein Token in einem Dokument auftritt, desto relevanter ist er für eine entsprechend
                    gestellte Suchanfrage. Eine weitere Problemstellung besteht in der <b>fehlenden Gewichtung</b> der Tokens
                    innerhalb einer Suchanfrage. Tokens, die in der Gesamtmenge der Dokumentmenge seltener vorkommen,
                    sind das Auffinden der gesuchten Informationen meist relevanter als häufig auftretende Tokens. </p>
            </details>

            <details>
                <summary><span>Vorkommenshäufigkeit</span></summary>

                <p>Die Vorkommenshäufigkeit eines Tokens $t$ in einem Dokument $d$ wird im Kontext von IR-Systemen als
                    Term Frequency $tf_{t,d}$ bezeichnet. Bei der Berücksichtigung der Vorkommenshäufigkeit für das
                    Ranking von Dokumenten bezüglich einer Suchanfrage ist zu beachten, dass im Allgemeinen <b>kein
                    linearer Zusammenhang zwischen der Vorkommenshäufigkeit eines Suchbegriffs und der Relevanz des
                        betreffenden Dokuments</b> besteht: Ein Dokument, das einen gesuchten Token zehnmal so häufig enthält
                    wie ein anderes, ist zwar relevanter, jedoch in der Regel nicht zehnmal so relevant. Um dieser
                    Tatsache Rechnung zu tragen, wird die Vorkommenshäufigkeit von Suchbegriffen bei der Berechnung der
                    Relevanz häufig mit einer <b>logarithmischen Dämpfung</b> einbezogen:</p>
                <center>$$w_{t,d} = \begin{cases} 1 + log_{10}(tf_{t,d}) \;\;\; tf_{t,d} > 0 \\ 0 \hspace{6.5em} \text{
                    sonst} \end{cases}$$
                </center>
                <p>Gegenüber dem Jaccard-Koeffizienten ermöglicht die logarithmische Gewichtung der Vorkommenshäufigkeit
                    gesuchter Tokens ein zielführenderes Ranking der Dokumente der Datenbasis in einem IR-System. Ein
                    wesentlicher Nachteil besteht hierbei jedoch darin, dass die <b>Gewichtung ausschließlich in der
                        Betrachtung einzelner Dokumente</b> erfolgt – eine Gesamtbetrachtung der Verteilung einzelner
                    Suchbegriffe über alle Dokumente der Datenbasis erfolgt nicht. Eine stärkere Gewichtung seltener,
                    aber relevanter Suchbegriffe ist auf diese Weise weiterhin nicht möglich.</p>
            </details>

            <details>
                <summary><span>TF-IDF-Gewichtung</span></summary>
                <p>Zur Bestimmung der Spezifität eines Suchbegriffs $t$ in Bezug auf die Gesamtmenge $N$ aller Dokumente
                    einer Datenbasis wird die <b>inverse Dokumentenhäufigkeit</b> (inverse document frequency) $idf_t$
                    bestimmt:</p>
                <center>$$idf_t = log_{10} \frac{N}{df_t}$$</center>
                <p>Der Term $df_t$ bezeichnet hierbei die <b>Dokumentenfrequenz</b> (document frequency) – die Anzahl der
                    Dokumente, in denen der Suchbegriff $t$ vorkommt. Genau wie bei der gewichteten Vorkommenshäufigkeit
                    $w_{t,d}$ erfolgt auch bei der Berechnung der inversen Dokumentenfrequenz eine logarithmische
                    Dämpfung. Je seltener ein Suchbegriff $t$ in der Gesamtmenge der Dokumente, desto höher ist seine
                    inverse Dokumentenfrequenz und damit seine Relevanz bezüglich des Auffindens relevanter
                    Informationen bezüglich $t$.</p>
                <p>In der Praxis werden die gewichtete Vorkommenshäufigkeit und die inverse Dokumentenhäufigkeit
                    gemeinsam für das Ranking von Dokumenten bezüglich einer Suchanfrage eingesetzt, um sowohl <b>lokale
                        Häufigkeiten</b> von Suchbegriffen in einzelnen Dokumenten, als auch ihre <b>globale Spezifität</b> bezüglich
                    der gesamten Datenbasis berücksichtigen zu können:</p>
                <center>$$w_{t_f - idf_{t,d}} = 1 + log_{10}(tf_{t,d}) \cdot log_{10} \frac{N}{df_t}$$</center>
            </details>
        </details>

        <details>
            <summary><span> Vector Space Model</span></summary>

            <p>Das Vector-Space-Modell beschreibt eine IR-Methode, bei welcher die <b>Dokumente einer Datenbasis in Form
                von Vektoren in einem hochdimensionalen Vektorraum gespeichert</b> werden. Jeder mögliche Suchbegriff ist
                hierbei durch eine eigene Dimension repräsentiert. Folglich ergeben sich die Komponenten der
                Dokumentvektoren aus den jeweiligen Gewichten der einzelnen Suchbegriffe. Als Metrik wird hierzu in der
                Praxis üblicherweise das <b>TF-IDF-Gewicht</b> verwendet. Alternativ können die Vektorkomponenten auch
                vereinfacht durch das Zählen der Vorkommenshäufigkeiten der jeweiligen Suchbegriffe bestimmt werden.
                Analog können <b>Suchanfragen ebenfalls in Vektoren</b> der Gewichte ihrer enthaltenen Tokens
                übersetzt werden. Das Ranking der Dokumente der Datenbasis ergibt sich anschließend aus ihrer
                Ähnlichkeit bezüglich des Vektors der Suchanfrage.</p>
            <p>Eine naheliegende Möglichkeit zur Bestimmung der Ähnlichkeit zweier Vektoren besteht in der Berechnung
                ihrer <b>Euklidischen Distanz</b>. Eine wesentliche Problemstellung besteht hierbei in der Berücksichtigung der
                unterschiedlichen Länge der Dokumente innerhalb der Datenbasis. Längere Dokumente verfügen tendenziell
                über eine größere Anzahl relevanter Suchbegriffe als kürzere; hieraus ergibt sich durch die höhere
                Anzahl von Null verschiedener Gewichtsfaktoren ebenfalls eine größere Länge des resultierenden
                Vektors.</p>

            <div class="card ovk-hinweis">
                <div class="card-header"><b>Beispiel:</b> Zwei übereinstimmende Dokumente</div>
                <div class="card-body">
                    <p>
                        Es werden zwei Dokumente betrachtet. Das Dokument $d_2$ enthält eine doppelte Kopie des Inhalts
                        aus Dokument $d_1$. Beide Dokumente verfügen damit weiterhin über denselben Informationsgehalt.
                        Da $d_1$ und $d_2$ über die gleichen individuellen Tokens verfügen, weisen sie bei der
                        Übersetzung in das Vector-Space-Modell den gleichen Winkel auf. Da alle Stichwörter in $d_2$
                        doppelt so häufig vorkommen wie in $d_1$, besitzt sein Vektor die doppelte Länge. Obwohl $d_1$
                        und $d_2$ semantisch identisch sind, beträgt ihre euklidische Distanz $|d_1|$.
                    </p>
                </div>
            </div>

            <p>Die Betrachtung des Beispiels verdeutlicht, dass der <b>Vergleich des Winkels zweier Vektoren</b> eine
                wesentlich robustere Möglichkeit zur Bestimmung ihrer Ähnlichkeit darstellt als die Ermittlung ihrer
                Euklidischen Distanz. Ein etabliertes Maß für die winkelbasierte Ähnlichkeitsbestimmung zweier Vektoren
                im Vector-Space-Modell ist die <b>Kosinus-Ähnlichkeit:</b></p>
            <center>$$cos_{q,d} = \frac{q \cdot d}{|q| \cdot |d|}$$</center>
            <p>Um die Berechnung der Kosinus-Ähnlichkeit zu vereinfachen, werden die betrachteten Vektoren üblicherweise
                durch die Anwendung der <b>Euklidischen Norm</b> auf die Länge 1 normiert:</p>
            <center>$$||x||_2 = \sqrt{\sum_i x_i^2} = 1$$</center>
            <p>Die Berechnung der Kosinus-Ähnlichkeit ergibt sich für die normierten Vektoren $q$ und $d$ damit zu:</p>
            <center>$$cos_{q, d} = q \cdot d$$</center>
        </details>

        <iframe src="https://information-retrieval-db-vorbereitungskurs-optla-610fbd847878ce.gitlab.io/" style="width: 100%; height: 310px; border: 1px solid #ced4da; border-radius: 10px; margin-top: 2em;"></iframe>

        {{> footer }}
    </div>
</div>
</body>
